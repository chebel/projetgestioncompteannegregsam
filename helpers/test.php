<?php
require_once "DateHelper.php";
$format = 'Y-m-d';
$date = DateHelper::toDateTimeWithFormat($format, '2009-02-15');
echo "Format: $format; " . $date->format('Y-m-d H:i:s') . "\n";