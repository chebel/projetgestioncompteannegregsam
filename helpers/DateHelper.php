<?php
class DateHelper
{
    public CONST DATE_FORMAT = "d-m-Y";

    public static function toDateTime(string $dateString) : ?DateTime
    {
        return self::dateFromFormat($dateString,self::DATE_FORMAT);
    }
    public static function toDateTimeWithFormat(String $dateAsString,string $dateFormat) : ?DateTime
    {
        // var_dump($dateAsString);
        return self::dateFromFormat($dateAsString,$dateFormat);
    }

    private static function dateFromFormat(string $dateAsString, string $dateFormat): ?DateTime
    {
        $date = \DateTime::createFromFormat($dateAsString, $dateFormat);
        // var_dump($dateAsString);
        // var_dump($dateFormat);
        // var_dump($date);

        if (false === $date) {
            return NULL;
        }
        list('warning_count' => $warning_count, 'error_count' => $error_count) = $date->getLastErrors();
        if (0 < $warning_count + $error_count) {
            return NULL;
        }
        return $date;
    }
}
