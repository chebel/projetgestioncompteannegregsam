<?php
require_once "services/dao/AdministrateurDao.php";
require_once "services/dto/Administrateur.php";
require_once "services/dto/Agence.php";
require_once "services/dao/AgenceDAO";

class CreerAdministrateurController
{
    
    private function validerChamp(string $pattern, $champ): bool
    {      
        if ($champ === null)
                return false;
        if (!is_object($champ) && gettype($champ) === "string")
            return preg_match($pattern, $champ);
        return true;
    }
}


?>