<?php
require_once "services/dao/ConseillerDao.php";
require_once "services/dto/Conseiller.php";

class CreerConseillerController
{
    
    private function validerChamp(string $pattern, $champ): bool
    {      
        if ($champ === null)
                return false;
        if (!is_object($champ) && gettype($champ) === "string")
            return preg_match($pattern, $champ);
        return true;
    }
    
    private ConseillerDao $conseillerDao;

    public function __construct()
    {
        $this->conseillerDao = new ConseillerDao();
    }
}
?>