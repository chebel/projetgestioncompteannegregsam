<?php

require_once "C:\wamp64\www\projetGestionClasseAnneGregSam\projetGestionCompteAnneGregSam\projetgestioncompteannegregsam/helpers/DateHelper.php";
class Conseiller
{

    private ?int $id;
    private ?string $nom;
    private ?string $prenom;
    private ?string $telephone;
    private ?string $email;
    private ?int $mdp;

    public function __construct(
        ?string $nom = null,
        ?string $prenom = null,
        ?string $telephone = null,
        ?string $email = null,
        ?int $mdp = null
    ) {


        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->telephone = $telephone;
        $this->email = $email;
    }





    /**
     * Get the value of id
     *
     * @return  mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param   mixed  $id  
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of numeroClient
     *
     * @return  mixed
     */
    public function getNumeroClient(): string
    {
        return $this->numeroClient;
    }

    /**
     * Set the value of numeroClient
     *
     * @param   mixed  $numeroClient  
     *
     * @return  self
     */
    public function setNumeroClient(string $numeroClient)
    {
        $this->numeroClient = $numeroClient;
    }

    /**
     * Get the value of nom
     *
     * @return  mixed
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param   mixed  $nom  
     *
     * @return  self
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    /**
     * Get the value of prenom
     *
     * @return  mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @param   mixed  $prenom  
     *
     * @return  self
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }


    /**
     * Get the value of telephone
     *
     * @return  mixed
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * Set the value of telephone
     *
     * @param   mixed  $telephone  
     *
     * @return  self
     */
    public function setTelephone(string $telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * Get the value of email
     *
     * @return  mixed
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @param   mixed  $email  
     *
     * @return  self
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }



    public function toArray(): array
    {
        $tab=[];
        $tab[]=$this->id;
        $tab[]=$this->nom;
        $tab[]=$this->prenom;
        $tab[]=$this->email; 
        return $tab;
    }

    public static function ConseillerFromArray(array $tab): ?conseiller
    {
        $Conseiller = new static();
        foreach ($tab as $key => $value) {
            $Conseiller->$key = $value;
        }
        return $Conseiller;
    }


    public static function  ConseillerEnterKeybord(): Conseiller
    {
        echo "Nouveau Conseiller : \n";
        $conseiller = new static();
        $conseiller->nom = readline("Nom ? ");
        $conseiller->prenom = readline("prenom ? ");
        $conseiller->telephone = readline("Telephone ? ");
        $conseiller->email = readline("Email ? ");
        return $conseiller;
    }
   
}

?>