<?php
require_once "C:\wamp64\www\GestionBanque/helpers/DateHelper.php";/*pas sur*/
class Administrateur
{
    private ?int $id;
    private ?string $nom;
    private ?string $prenom;
    private ?string $telephone;
    private ?string $email;
    private ?string $mdp;

    public function __construct(
        ?string $nom = null,
        ?string $prenom = null,
        ?string $telephone = null,
        ?string $email = null,
        ?string $mdp = null

    ) {
        $this->id = null;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->telephone = $telephone;
        $this->email = $email;
        $this->mdp = $mdp;
    }
      /**
     * Get the value of id
     *
     * @return  mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

      /**
     * Set the value of id
     *
     * @param   mixed  $id  
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

     /**
     * Get the value of nom
     *
     * @return  mixed
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param   mixed  $nom 
     *
     * @return  self
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

     /**
     * Get the value of prenom
     *
     * @return  mixed
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @param   mixed  $prenom 
     *
     * @return  self
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }


       /**
     * Get the value of telephone
     *
     * @return  mixed
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

      /**
     * Set the value of telephone
     *
     * @param   mixed  $telephone 
     *
     * @return  self
     */
    public function setTelephone(string $telephone)
    {
        $this->telephone = $telephone;
    }

     /**
     * Get the value of email
     *
     * @return  mixed
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @param   mixed  $email 
     *
     * @return  self
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

     /**
     * Get the value of mdp
     *
     * @return  mixed
     */
    public function getMdp(): string
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @param   mixed  $mdp 
     *
     * @return  self
     */
    public function setMdp(string $mdp)
    {
        $this->mdp = $mdp;
    }


  
         public function toArray(): array
    {
        $tab=[];
        $tab[]=$this->id;
        $tab[]=$this->nom;
        $tab[]=$this->prenom;
        $tab[]=$this->email;
        $tab[]=$this->mdp; 
        return $tab;
    }

    public static function AdminFromArray(array $tab): ? Administrateur
    {
        $administrateur = new static();
        foreach ($tab as $key => $value) {
            $administrateur->$key = $value;
        }
        return $administrateur;
    }


    public static function  AdminEnterKeybord(): Administrateur
    {
        echo "Nouveau Client : \n";
        $administrateur = new static();
        $administrateur->nom = readline("Nom ? ");
        $administrateur->prenom = readline("prenom ? ");
        $administrateur->telephone = readline("Telephone ? ");
        $administrateur->email = readline("Email ? ");
        return $administrateur;
    }
   
}
    
?>