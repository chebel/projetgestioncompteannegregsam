<?php
require_once "ConstantesDao.php";
require_once "ClientDao";
require_once "CompteDao.php";

class ConseillerDao
{
    private const FILE_SAVE_COMPTE = "c:/envdev/donnees/save_comptes.csv";
    private const FILE_CPT_COMPTE = "c:/envdev/donnees/compteurs/cpt_comptes.txt";
    private const CHAMP_ID = "id";
    private const CHAMP_TYPE = "type";
    private const CHAMP_DOMICILE = "domicile";
    private const CHAMP_CLIENT = "id_client";
    private const CHAMP_AGENCE = "id_agence";


    private ClientDao $clientDao;

    public function __construct()
    {
        $this->clientDao = new ClientDao();
    }

    private const ENTETES_CONSEILLE = [
        ConseillerDao::CHAMP_ID,
        ConseillerDao::CHAMP_TYPE,
        ConseillerDao::CHAMP_CLIENT,
        ConseillerDao::CHAMP_AGENCE,
        ConseillerDao::CHAMP_DOMICILE
    ];
}

?>